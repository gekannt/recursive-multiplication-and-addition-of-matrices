#include <iostream>
#include <stdio.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <algorithm>
#include <vector>
using namespace std;

#include "macro_info_handling.h"

int test_check(char *file_name)
{
    FILE *input=NULL;
    char *block=NULL;

//    block=malloc(100);

}

void test_debug()
{   debug("I have black hair");
    debug("I have %d notebooks",56);

}

void test_log_err()
{
    log_err("I think everything is broken");
    log_err("there are %d problems in %s",0,"space");
}

void test_log_warn()
{
    log_warn("you can safely ignore this");
    log_warn("there are %d prblems in %s %d",0,"space",5);

}

void test_log_info()
{
    log_info("well I did something mundane");
    log_info(" it happened %f times today",1.3f);
}

int arr[]={1,10,3};

int glob;

int sum(int *arr,int number)
{  int val=0;

    if (number > 1)
     {  number--;
        val+=sum(arr+1,number);
     }

   return *arr+val;
}

int arr1[]={1,2,3,4,5};
int arr2[]={1,2,3,4,5};
int result[]={0,0,0,0,0};



int sum_matr(int *arr1, int *arr2, int first_number, int second_number,int *result)
{
 #define SOME_RANDOM_VALUE 3
 int indicator=SOME_RANDOM_VALUE;

 static int depth;
 depth++;
 int loc=depth;  // every level ought to have own level of depth

 if ( first_number >1 || second_number>1) // make it smooth
 {

     if ( first_number >second_number) // for the sake of the evenness of diving
     {
         first_number--;
         sum_matr(arr1+1,arr2,first_number,second_number,result);
   #define ANOTHER_RANDOM 2
         indicator=ANOTHER_RANDOM;
     }
     else
     {
     #define YET_ONE_RANDOM 1
        second_number--;
        sum_matr(arr1,arr2+1,first_number,second_number,result);
        indicator=YET_ONE_RANDOM;
     }
 }
 else
     indicator=1; //  root of the recursion

 if ( indicator == 1)
 { int dd=*arr1+*arr2; // for watching via debugger
   #define QUANTITY_OF_ARRAY_AND_RESPECTIVELY_DIVINGS 2
   int index=loc/QUANTITY_OF_ARRAY_AND_RESPECTIVELY_DIVINGS;
   result[index]=dd;

  #define SOMETHING_UNIMPORTANT 0
   return SOMETHING_UNIMPORTANT;
 }

 return SOMETHING_UNIMPORTANT;

}


void print(int a)
{
    cout<<a<<" ";
}


int matr1[]={1,2,3,  4,5,6,  7,8,9};  // 1 2 3
                              // 4 5 6
                              // 7 8 9

int matr2[]={1,2,3,  4,5,6,  7,8,9};   // 1 2 3
                                 // 4 5 6
                                 // 7 8 9
int matr_result[]={0,0,0, 0,0,0, 0, 0,0};

// watch after alignment before passing matrices
//void matr_multiplication(int *a, int *b, int am, int an, int bm, int bn)
//{
//   static int depth;
//   depth++;
//    if ( am !=0)
//    {   am--;
//        matr_multiplication(a+an,b,am,an,bm,bn);
//    }

//    if ( depth>an)
//    {   depth--;
//        return;
//    }
//    int arr[10]={0,};
//    memcpy(arr,a,an*sizeof(int));
//    memcpy(arr,a,an);
//}


int *transpose(int *array,int m,int n)
{
    #define SIZE_OF_MATR m*n
    int *matr_new=(int *) malloc(m * n *(sizeof(int) ));

    for (int i=0; i<n;i++)
     for (int j=0; j<m;j++)
         matr_new[i*m+j]=array [i+j*m];

    return matr_new;
}


long long int multiplication_on_the_another_processor(int *a, int *b,int bm) // send on the another processor
{
    long long int accum=0;
    // an == bm here , otherwise multiplication is impossible
    for (int i=0;i<bm;i++)
      accum=a[i]*b[i]+accum;
    return accum;
}

// watch after alignment before passing matrices
void matr_multiplication(int *a, int *b, int am, int an, int bm, int bn)
{
   static int depth;
   int attr=3;
  depth=am;

   if (depth !=1 || bn!=0)
   {
    if (depth >1)  // control element and must be entered only once, so it's static
    {   depth--;
        matr_multiplication(a+an,b,depth,an,bm,bn);

    }

    if (bn >1)
     {  bn--;
        matr_multiplication(a,b+bm,depth,an,bm,bn);
        attr=1;
     }
   }
   else
     return;

   static int count=bm*an; // total number of elements
    count--;
    matr_result[count]=multiplication_on_the_another_processor(a,b,bm);

}



void func()
{
    int a=0;
    while(true)
    {
       a++;
       if ( a>2)
       {
          a=0;
           break;
       }



       cout<<a;
    }


    cout<<a<<"\n";
}


int main(int argc, int *argv[])
{

    func();

    return 0;

 int a[]={1,2,3,4};
 int *p1=a,*p2=NULL;



 p2=p1;
 p1++;
 cout<<*p2<<"  "<<*p1;




    return 0;

   int *tt;
   tt=transpose(matr2,3,3);

   for_each(tt,tt+9,print);
   cout<<"\n";
  matr_multiplication(matr1,tt,3,3,3,3);
//    matr_multiplication(matr1,tt,3,3,3,3);


  for_each(matr_result,matr_result+9,print);
    free(tt);
  return 0;




  //ADDITION


//    cout<<sum(arr,3)<<"\n";
//    cout<<glob<<"\n";
#define NUMBER_OF_EL 5

    sum_matr(arr1,arr2,NUMBER_OF_EL,NUMBER_OF_EL,result);

    for_each(result,result+NUMBER_OF_EL,print);
     cout<<"\n";
//    string line;

//   argc=2;
//    check(argc==0,"need an argument");
//    check_mem(0)

//   log_err("mistake, oh this line");
//    test_log_err();
// test_log_warn();
// test_log_info();

  return 0;

  error:
   return -1;
}
